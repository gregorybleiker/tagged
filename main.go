/*
Copyright © 2023 NAME HERE <EMAIL ADDRESS>

*/
package main

import "tagged/cmd"

func main() {
	cmd.Execute()
}
